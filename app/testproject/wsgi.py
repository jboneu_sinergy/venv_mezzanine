from __future__ import unicode_literals

import os
import sys
sys.path.insert(0, os.path.abspath('/srv/mezzanine_project/app/'))
#os.environ['DJANGO_SETTINGS_MODULE'] = 'testproject.settings'

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
settings_module = "%s.settings" % PROJECT_ROOT.split(os.sep)[-1]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings_module)

from django.core.wsgi import get_wsgi_application
#sys.path.insert(0, os.path.abspath('/srv/mezzanine_project/venv/lib/python2.7/site-packages/'))
sys.path.insert(0, os.path.abspath('/srv/mezzanine_project/venv/local/lib/python2.7/site-packages/'))

msg =  str(sys.path)
application = get_wsgi_application()
